# Proyecto de Ingeniería de Software II - II Ciclo - 2018

---

## Beneficiario del proyecto
Fundecooperación para el Desarrollo Sostenible, es una fundación sin fines de lucro con representación multisegmental que trabaja bajo un modelo socio-ambiental donde se integra al emprendedor o microempresario que tiene limitado acceso a mecanismos de financiamiento apropiados a sus necesidades y contexto socioeconómico.

En la actualidad Fundecooperación se ha propuesto como meta la promoción del desarrollo sostenible en Costa Rica; mediante el financiamiento y apoyo a emprendedores y PYMES en todo el país, mediante el establecimiento de sus planes de negocios con oportunidad de mejora ambiental.

La Fundación tiene el Programa “Crédito a su Medida” para MIPYMES y es parte de la Red Costarricense de Organizaciones de Microfinanzas (REDCOM) en Costa Rica, de manera que otorga préstamo a iniciativas productivas sostenibles y actualmente están buscando automatizar sus procesos para agilizar los trámites y tiempos de atención, ampliar su área de impacto y los montos de colocación.

### Misión

Trabajamos por la mejora de las condiciones socio productivas, ambientales y de género de la población en Costa Rica, para un desarrollo económico y social respetuoso con el medio ambiente, brindando financiamiento a emprendedores y empresarios.

### Visión

Ser el referente en Costa Rica en financiamiento de proyectos en desarrollo sostenible, que generan impacto positivo para un desarrollo económico y social, que sea respetuoso con el medio ambiente.

### Contacto

* Sitio web: http://fundecooperacion.org/
* Product Owner: Andrea Matarrita 
* Cel: +(506)8845-2665
* Correo: amatarrita@fundecooperacion.org

---

## Proyecto

### Contexto

Uno de los públicos meta del programa de crédito son las comunidades costarricenses con líneas de pobreza básica y extrema, que a través del apoyo del FIDEIMAS pueden recibir avales de garantía para sacar adelante su economía familiar. Para esto se requiere automatizar un proceso y herramienta de análisis para simplificar los trabajos en campo ya que Fundecooperación labora a nivel nacional con únicamente 4 asesores de crédito.

El proceso de valoración requiere realizar un análisis técnico y un análisis financiero para validar la capacidad de pago de los solicitantes de crédito. Los pasos a seguir dentro de este procedimiento son los siguientes [entre paréntesis el tiempo promedio en días que se necesita para pasar al siguiente paso]

    1. El potencial cliente completa una solicitud de crédito (físico o en línea).
    2. Se realiza un pre-análisis por parte del asesor(a) de crédito.
            [Pasos 1 y 2 tardan en promedio 7 días hábiles]
    3. Se hace una visita técnica en el sitio para recolectar información necesaria para hacer el análisis técnico y financiero (se realizan encuestas, las cuales se llenan en papel)
    4. Se alimenta la base de datos de forma manual (una parte se carga al sistema de crédito de la Fundación “SALESFORCE” y otra a una hoja de cálculo de Excel) con la información recuperada y se realiza en análisis técnico y financiero.
            [Pasos 3 y 4 tardan un promedio de 10 días hábiles]
    5. Se crea una ficha técnica, junto con un resumen financiero y se realiza una presentación resumen al nivel de resolución de crédito respectivo (hay 4 niveles, según el monto del crédito que va desde c500mil a c75millones). 
            [Paso 5 tarda un promedio de 1 día hábil]
    6. Se dicta una resolución de acuerdo al análisis técnico y financiero.
            [Paso 6 tarda un promedio de 1 día hábil]

La fundación busca que el proceso de otorgar préstamos sea en el menor tiempo posible, por lo que ha ido automatizando y desarrollando un sistema propio, para ello utiliza el software CRM Salesforce la cual cuenta con varias herramientas para la customización del manejo de la información.

### Expectativas

    1. La duración promedio para realizar las encuestas en campo sea de máximo 1 día (dada la cobertura geográfica que abarca la Fundación para colocar sus créditos).
    2. Un solo empleado pueda visitar 4 cantidad de clientes en un día.
    3. La información recopilada se carga automáticamente al sistema principal de crédito (SALESFORCE).
    4. El análisis técnico se realiza de forma automática en el momento que se suba la información al sistema.
    5. El tiempo promedio de duración de los pasos 3 y 4 baje a 2 días hábiles (por cuestiones de conexión a internet).

### Requerimientos mínimos

    1. Crear una aplicación móvil personalizada nativa para android utilizando herramientas de salesforce para su integración con el sistema principal (compatible con android 6.0 y siguientes, la herramienta a utilizar se denomina Movile SDK)
    2. La aplicación móvil consta de una encuesta la cual debe de tener una interfaz amigable para el usuario (desde personas con estudios básicos hasta técnicos, el enfoque es que pueden utilizar sin necesidad de manuales)
    3. La aplicación móvil debe ser capaz de guardar los datos de la encuesta en el teléfono/tableta en modo offline.
    4. La aplicación móvil debe de enviar la información al sistema principal utilizando integración en la nube del software SALESFORCE una vez que el dispositivo móvil se encuentre conectado a una red de datos.
    5. Se creará una aplicación (inclusive página) en SALESFORCE la cual mediante cálculos matemáticos y la información recopilada en la encuesta, presentará un diagrama araña y presentar los resultados de manera automática para su aprobación en línea.

---

## Recursos web

### Android

#### Online
* Completar el tutorial en: https://developer.android.com/training/basics/firstapp/
* Leer la guía Android en: https://developer.android.com/guide/components/fundamentals

### Forcedroid

#### SDK
* Instrucciones de instalación en: https://www.npmjs.com/package/forcedroid

### Proyecto

#### Documentación
* Documentos y plantillas: https://goo.gl/WjjFCK

#### Accesos
* SalesForce de producción: http://ovh.to/J48LygD
* SandBox para desarrollo: https://cs59.salesforce.com/?un=amatarrita%40fundecooperacion.org.aleman 
* Usuario: amatarrita@fundecooperacion.org.aleman
* Clave: SHINJI1985

* Bitácora: https://docs.google.com/document/d/1MoIhppzI2hxy9a-B8CYvr71WJn49LH9emwSpERmyJ3Y/edit?usp=sharing
---
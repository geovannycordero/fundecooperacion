package fundecooperacion.org;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.ui.SalesforceActivity;

import fundecooperacion.org.Utils.SalesForceSOQL;

public class FirstPage extends SalesforceActivity {
    private RestClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_page);
    }

    @Override
    public void onResume(RestClient client) {
        // Keeping reference to rest client
        this.client = client;
        SalesForceSOQL.client = client;
    }

    public void openInfo(View view){
        Intent intent = new Intent(getApplicationContext(), Information.class);
        startActivity(intent);
    }

    public void openClientsActivity(View v){
        Intent myIntent = new Intent(getApplicationContext(), ClientsPage.class);
        startActivityForResult(myIntent, 0);
    }
}

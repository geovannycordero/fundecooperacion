package fundecooperacion.org;

import java.util.HashMap;

/**
 * Created by b11391 on 15/11/2018.
 */

public class Answers {
    public static final HashMap<String, HashMap<String, HashMap<String, Float>>> answers = new HashMap<>();

    public static HashMap<String, HashMap<String, Float>> currentUser;

    public static HashMap<String, Float> currentPoll;

    public static void setCurrentUser(String user) {
        currentUser = answers.get(user);
        if (currentUser == null) {
            HashMap<String, HashMap<String, Float>> tempUser = new HashMap<>();
            answers.put(user, tempUser);
            currentUser = tempUser;
        }
    }

    public static void setCurrentPoll(String pollId) {
        currentPoll = currentUser.get(pollId);
        if (currentPoll == null) {
            HashMap<String, Float> tempPoll = new HashMap<>();
            currentUser.put(pollId, tempPoll);
            currentPoll = tempPoll;
        }
    }
}

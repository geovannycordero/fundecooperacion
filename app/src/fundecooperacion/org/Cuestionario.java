package fundecooperacion.org;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import fundecooperacion.org.Utils.Questionnaire;

import java.util.*;


public class Cuestionario extends AppCompatActivity {

    boolean ultimo = false;
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    private HashMap<Integer, Integer> myAnswers;
    private TextView textView;
    private Button b_finalizar;
    private Button b_expandir;
    private String questionnaireName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuestionario);

        Intent intent = getIntent();
        final ArrayList<String> cuestionarios = (ArrayList<String>) intent.getSerializableExtra("cuestionarios");

        // Guarda respuestas, grupo, child
        myAnswers = new HashMap<Integer, Integer>();

        // lleva control de indices que cambiaron de color
        final List<Integer> posicionColor = new ArrayList<>();

        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        textView = (TextView) findViewById(R.id.t_seccion);

        questionnaireName = cuestionarios.remove(0);

        textView.setText(questionnaireName);

        if (cuestionarios.isEmpty()) ultimo = true;

        // Boton expandir / finalizar (contraer)
        b_finalizar = (Button) findViewById(R.id.b_finalizar);
        b_expandir = (Button) findViewById(R.id.b_expandir);

        // preparing list data
        prepareListData((String) textView.getText());

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Expandir
        b_expandir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = listDataHeader.size();//expListView.getCount();

                for (int i = 0; i < count; i++) {             // poco eficiente
                    if (!expListView.isGroupExpanded(i)) {
                        expListView.expandGroup(i);
                    }
                }
            }
        });

        // contraer / finaliza
        b_finalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View vista;
                ViewGroup padre;
                boolean completo = true;
                int index = 0;
                int count = listDataHeader.size();//expListView.getCount();
                for (int i = 0; i < count; i++) {
                    if (!myAnswers.containsKey(i)) {
                        index = expListView.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(i));

                        expListView.getChildAt(index).setBackgroundColor(Color.rgb(255, 58, 52));
                        completo = false;

                    } else {
                        index = expListView.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(i));
                        expListView.getChildAt(index).setBackgroundColor(Color.rgb(89, 255, 131));

                    }
                    if (expListView.isGroupExpanded(i)) {
                        expListView.collapseGroup(i);
                    }
                }

                if (completo) {
                    //Cuestionario terminado
                    HashMap<String, String> respuestas = new HashMap<String, String>();

                    Iterator entries = myAnswers.entrySet().iterator();
                    while (entries.hasNext()) {
                        Map.Entry entry = (Map.Entry) entries.next();
                        Integer key = (Integer) entry.getKey();
                        Integer value = (Integer) entry.getValue();

                        respuestas.put(listDataHeader.get(key), listDataChild.get(listDataHeader.get(key)).get(value));
                    }

                    // Enviar a siguiente
                    Intent intent;
                    //cuestionarios.put((String) textView.getText(), respuestas);
                    if (!ultimo) {
                        intent = getIntent();
                        intent.putExtra("cuestionarios", cuestionarios);
                    } else {
                        intent = new Intent(getApplicationContext(), Guardar.class);
                        intent.putExtra("cuestionarios", cuestionarios);
                    }
                    startActivity(intent);
                }
            }
        });
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            int _lastColored;

            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                int index = 0;
                if (myAnswers.containsKey(groupPosition)) {
                    _lastColored = myAnswers.get(groupPosition);
                    index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, _lastColored));
                    //parent.getChildAt(index).setBackgroundColor(Color.TRANSPARENT);

                    myAnswers.remove(groupPosition);
                }

                myAnswers.put(groupPosition, childPosition);
                // color a hijo
                // color a padre

                String selectedOption = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                Answers.currentPoll.put(listDataHeader.get(groupPosition), Questionnaire.questionnaire.get(questionnaireName).get(listDataHeader.get(groupPosition)).get(selectedOption));

                index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(groupPosition));
                parent.getChildAt(index).setBackgroundColor(Color.rgb(89, 255, 131));
                posicionColor.add(index);

                parent.collapseGroup(groupPosition);

                return false;
            }

        });

    }

    /*
     * Preparing the list data
     */
    private void prepareListData(String seccion) {

        HashMap<String, HashMap<String, Float>> q = Questionnaire.questionnaire.get(seccion);

        listDataHeader = new ArrayList<String>(q.keySet());
        listDataChild = new HashMap<String, List<String>>();

        for (String s : listDataHeader)
            listDataChild.put(s, new ArrayList<String>(q.get(s).keySet()));

        Answers.setCurrentPoll(questionnaireName);
    }
}
package fundecooperacion.org;

import android.app.Application;
import com.salesforce.androidsdk.analytics.security.Encryptor;
import com.salesforce.androidsdk.app.SalesforceSDKManager.KeyInterface;
import com.salesforce.androidsdk.smartsync.app.SmartSyncSDKManager;

/**
 * Application class for our application.
 */
public class MainApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		SmartSyncSDKManager.initNative(getApplicationContext(), new NativeKeyImpl(), FirstPage.class);
	}
}

class NativeKeyImpl implements KeyInterface {
	@Override
	public String getKey(String name) {
		return Encryptor.hash(name + "12s9adpahk;n12-97sdainkasd=012", name + "12kl0dsakj4-cxh1qewkjasdol8");
	}
}

package fundecooperacion.org;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.salesforce.androidsdk.app.SalesforceSDKManager;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;
import fundecooperacion.org.Utils.SalesForceSOQL;

import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


public class Guardar extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Intent intent = getIntent();
        //final HashMap<String, HashMap<String, String>> cuestionarios = (HashMap<String, HashMap<String, String>>) intent.getSerializableExtra("cuestionarios");

        setContentView(R.layout.activity_guardar);
        Gson gson = new Gson();

        Log.v("Guardar", gson.toJson(Answers.answers));
    }

    public void clickButtomOK(View v) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_popup_disk_full)
                .setTitle("Cargar datos de la encuesta")
                .setMessage("Operación es irreversible. ¿Desea continuar?")
                .setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        upsertAll();
                        Intent intent = new Intent(getApplicationContext(), FirstPage.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        /***********************************************************************/
                        Toast.makeText(getApplicationContext(), "Se perderan los datos recolectados anteriormente!", Toast.LENGTH_LONG).show();

                        /***********************************************************************/
                    }
                })
                .show();
    }

    private void upsertAll() {
        for (HashMap.Entry<String, HashMap<String, HashMap<String, Float>>> entry : Answers.answers.entrySet()) {

            if (entry.getValue() != null) {
                HashMap<String, Object> fields = new HashMap<>();

                for (HashMap.Entry<String, HashMap<String, Float>> eee : entry.getValue().entrySet())
                    fields.put(eee.getKey(), average(eee.getValue().values()));

                Gson gson = new GsonBuilder().create();

                Log.i("UPSERT", gson.toJson(fields));

                HashMap<String, Object> res = new HashMap<>();
                res.put("EntrevistaJSON__c", gson.toJson(fields));
                try {
                    upsertData(res, "Entrevistas__c", "AccountId__c", entry.getKey());
                } catch (UnsupportedEncodingException e) {
                    Log.e("upsert", "UnsupportedEncodingException\n" + e.toString());
                }
            }
        }
    }

    private Float average(Collection<Float> collectionFloat) {
        int count = collectionFloat.size();
        Float res = new Float(0);
        for (Float a : collectionFloat) {
            res += a;
        }
        return res / count;
    }

    private void upsertData(Map<String, Object> fields, String objectType, String externalIdField, String externalId) throws UnsupportedEncodingException {
        final RestRequest restRequest = RestRequest.getRequestForUpsert(getString(R.string.api_version), objectType, externalIdField, externalId, fields);
        final String externalId2 = externalId;
        SalesForceSOQL.client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {

            @Override
            public void onSuccess(RestRequest request, final RestResponse result) {
                result.consumeQuietly(); // consume before going back to main thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Toast.makeText(Guardar.this, "Datos cargados correctamente", Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            onError(e);
                        }
                    }
                });
            }

            @Override
            public void onError(final Exception exception) {
                Log.i("UPSERT", restRequest.getRequestBodyAsJson().toString());

                String fileContents = restRequest.getRequestBodyAsJson().toString();
                FileOutputStream outputStream;

                try {
                    outputStream = openFileOutput(externalId2, Context.MODE_PRIVATE);
                    outputStream.write(fileContents.getBytes());
                    outputStream.close();
                } catch (final Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(Guardar.this,
                                    Guardar.this.getString(SalesforceSDKManager.getInstance().getSalesforceR().stringGenericError(), e.toString()),
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(Guardar.this,
                                "No Internet connection available at the moment\nThe answers will be uploaded later",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }
}

package fundecooperacion.org;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import fundecooperacion.org.Utils.Questionnaire;

import java.util.ArrayList;

public class ListaCuestionarios extends AppCompatActivity {

    ArrayList<DataModel> dataModels;
    ListView listView;
    private CustomAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.questionary_list);

        Intent intent = getIntent();
        String message = intent.getStringExtra(ClientsPage.EXTRA_MESSAGE);

        TextView textView = findViewById(R.id.name_field);
        textView.setText(message);

        listView = (ListView) findViewById(R.id.questionnaire_list);

        dataModels = new ArrayList<>();

        Questionnaire.loadQuestionnaire(this);

        for (String s : Questionnaire.questionnaire.keySet())
            dataModels.add(new DataModel(s, false));


        adapter = new CustomAdapter(dataModels, getApplicationContext());

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {
                DataModel dataModel = dataModels.get(position);
                dataModel.checked = !dataModel.checked;
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void nextClick(View v) {
        ArrayList<String> cuestionarios = new ArrayList<>();

        for (DataModel dataModel : dataModels) {
            if (dataModel.checked) cuestionarios.add(dataModel.name);
        }

        if (cuestionarios.size() != 0) {
            Intent myIntent = new Intent(getApplicationContext(), Cuestionario.class);
            myIntent.putExtra("cuestionarios", cuestionarios);
            startActivityForResult(myIntent, 0);
        } else {
            Toast.makeText(getApplicationContext(), "Debe seleccionar al menos una actividad!", Toast.LENGTH_LONG).show();
        }
    }

    public class DataModel {

        public String name;
        boolean checked;

        DataModel(String name, boolean checked) {
            this.name = name;
            this.checked = checked;
        }
    }

    public class CustomAdapter extends ArrayAdapter {

        Context mContext;
        private ArrayList<DataModel> dataSet;

        public CustomAdapter(ArrayList data, Context context) {
            super(context, R.layout.row_item, data);
            this.dataSet = data;
            this.mContext = context;

        }

        @Override
        public int getCount() {
            return dataSet.size();
        }

        @Override
        public DataModel getItem(int position) {
            return dataSet.get(position);
        }

        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

            ViewHolder viewHolder;
            final View result;

            if (convertView == null) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
                viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
                viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);

                result = convertView;
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
                result = convertView;
            }

            DataModel item = getItem(position);

            viewHolder.txtName.setText(item.name);
            viewHolder.checkBox.setChecked(item.checked);
            viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DataModel dataModel = dataModels.get(position);
                    dataModel.checked = !dataModel.checked;
                    adapter.notifyDataSetChanged();
                    adapter.notifyDataSetChanged();
                }
            });
            return result;
        }

        // View lookup cache
        private class ViewHolder {
            TextView txtName;
            CheckBox checkBox;
        }
    }
}
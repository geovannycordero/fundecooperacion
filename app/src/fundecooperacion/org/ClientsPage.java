package fundecooperacion.org;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.salesforce.androidsdk.rest.ApiVersionStrings;
import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;
import org.json.JSONArray;

import static fundecooperacion.org.Utils.SalesForceSOQL.client;

public class ClientsPage extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = " ";

    public String clientName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clients_page);
        this.loadListInfo();
    }

    public void loadListInfo() {
        ListView listView = (ListView) findViewById(R.id.clients_list);

        final ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1) {
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                // Get the data item for this position
                User user = getItem(position);
                // Check if an existing view is being reused, otherwise inflate the view
                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_user, parent, false);
                }
                TextView userName = convertView.findViewById(R.id.item_user);
                userName.setText(user.name);
                return convertView;
            }
        };
        final ProgressBar progressBar = findViewById(R.id.progress_bar);

        try {
            RestRequest restRequest =
                    RestRequest.getRequestForQuery(ApiVersionStrings.getVersionNumber(this), "SELECT Name , ID FROM Account");

            client.sendAsync(restRequest, new RestClient.AsyncRequestCallback() {
                @Override
                public void onSuccess(final RestRequest request, final RestResponse result) {
                    result.consumeQuietly(); // consume before going back to main thread
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONArray records = result.asJSONObject().getJSONArray("records");

                                for (int i = 0; i < records.length(); i++) {
                                    //acá se añade el nombre de los users
                                    String tempName = records.getJSONObject(i).getString("Name");
                                    String tempId = records.getJSONObject(i).getString("Id");
                                    adapter.add(new User(tempName, tempId));
                                }
                                progressBar.setVisibility(View.INVISIBLE);
                            } catch (Exception e) {

                            }
                        }
                    });
                }

                @Override
                public void onError(final Exception exception) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(ClientsPage.this, "No se logro establecer comunicacion con el servidor, cargando archivos locales.", Toast.LENGTH_LONG).show();
                            loadSavedData(adapter);
                        }
                    });
                }
            });
        } catch (Exception e) {
            progressBar.setVisibility(View.INVISIBLE);
            Toast.makeText(ClientsPage.this, "No se logro establecer comunicacion con el servidor, cargando archivos locales.", Toast.LENGTH_LONG).show();
            loadSavedData(adapter);
        }

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                User tempUser = (User) adapterView.getItemAtPosition(i);
                clientName = tempUser.name;

                Answers.setCurrentUser(tempUser.id);
            }
        });
    }

    public void openActivitiesActivity(View view) {
        Intent intent = new Intent(getApplicationContext(), ListaCuestionarios.class);

        final ListView listView = (ListView) findViewById(R.id.clients_list);

        intent.putExtra(EXTRA_MESSAGE, clientName);
        startActivity(intent);
    }

    public void loadSavedData(ArrayAdapter<User> adapter) {
        adapter.add(new User("Client X", "12345"));
        adapter.add(new User("Client 4", "12345"));
        adapter.add(new User("Client 3", "12345"));
        adapter.add(new User("Client 1", "12345"));
        adapter.add(new User("Client 5", "12345"));
    }

    class User {
        public final String name;
        public final String id;

        User(String name, String id) {
            this.name = name;
            this.id = id;
        }

    }
}
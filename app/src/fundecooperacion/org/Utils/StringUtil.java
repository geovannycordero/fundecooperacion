package fundecooperacion.org.Utils;

import java.util.HashMap;
import com.google.gson.*;
import com.salesforce.androidsdk.rest.RestClient;

/**
 * Created by b52368 on 15/11/2018.
 */

public class StringUtil {

    /**
     * Converts HashMap to JSON string
     * @param map the hashMap to be cast
     * @return new String in JSON format
     */
    public static String hashToJSON (HashMap<String,HashMap<String,String>> map){
        return new Gson().toJson(map);
    }
}

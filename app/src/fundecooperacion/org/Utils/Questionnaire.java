package fundecooperacion.org.Utils;

import android.content.Context;
import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fundecooperacion.org.R;

import java.lang.reflect.Type;
import java.util.HashMap;

public class Questionnaire {
    public static HashMap<String, HashMap<String, HashMap<String, Float>>> questionnaire;

    public static void loadQuestionnaire(Context context) {
        String s = context.getString(R.string.questionnaire);
        Log.i("string", s);
        Type qtype = new TypeToken<HashMap<String, HashMap<String, HashMap<String, Float>>>>() { }.getType();
        Gson g = new Gson();
        questionnaire = g.fromJson(s, qtype);
    }
}

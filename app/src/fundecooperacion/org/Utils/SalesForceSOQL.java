package fundecooperacion.org.Utils;

import com.salesforce.androidsdk.rest.RestClient;
import com.salesforce.androidsdk.rest.RestRequest;
import com.salesforce.androidsdk.rest.RestResponse;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by b52368 on 15/11/2018.
 */

public class SalesForceSOQL {

    public static RestClient client;

    /**
     * This method calls for a query sync
     * @param apiVersion api version
     * @param soql query in soql language
     * @return An JSONObject with the response. Can return null if no response is obtained
     * @throws UnsupportedEncodingException
     */
    public static JSONObject sendRequest(String apiVersion, String soql) throws UnsupportedEncodingException {
        RestRequest restRequest = RestRequest.getRequestForQuery(apiVersion, soql);
        try{
            RestResponse response = client.sendSync(restRequest);
            return response.asJSONObject();
        } catch (Exception e){
            return null;
        }
    }

    // https://forcedotcom.github.io/SalesforceMobileSDK-Android/com/salesforce/androidsdk/rest/RestRequest.html#getRequestForUpsert-java.lang.String-java.lang.String-java.lang.String-java.lang.String-java.util.Map-
    //Probar con el insert de los datos
    // Ejemplo acá: https://developer.salesforce.com/forums/?id=906F00000009EEKIA2
    /**
     *
     * @param apiVersion
     * @param fields
     * @param objectType
     * @param externalIdField
     * @param externalId
     * @return
     * @throws UnsupportedEncodingException
     */
    public static boolean upsertData(String apiVersion, Map<String, Object> fields, String objectType, String externalIdField, String externalId) throws UnsupportedEncodingException {
        RestRequest restRequest =
                RestRequest.getRequestForUpsert(apiVersion, objectType, externalIdField, externalId, fields);
        try{
            client.sendSync(restRequest);
            return true;
        } catch (Exception e){
            return false;
        }
    }

}
